//
//  ResultOnTime.swift
//  ProjectBeginning
//
//  Created by Antonina Nahnybida on 11/14/16.
//  Copyright © 2016 Andrew Nahnybida. All rights reserved.
//

import UIKit

class ResultOnTime: UIViewController {
    
    var score = 0
    
    @IBAction func playAgain() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backToMenu() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func checkStats() {
        
        
    }
    
    @IBOutlet weak var showerSchet: UILabel!
    
    @IBOutlet weak var phraseAfterGame: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showerSchet.text = score.description
        
        if score <= 25 {
            phraseAfterGame.text = "You need to practice a bit more!"
        } else {
            if score > 26 {
                phraseAfterGame.text = "You are doing quite well in this mode!"
            } else {
                if score > 60 {
                    phraseAfterGame.text = "You are a monester in this mode!"
                } else {
                    if score > 100 {
                        phraseAfterGame.text = "You have ENTERED GOD MODE!"
                    }
                }
            }
        }
    }
}

//
//  Statistics.swift
//  ProjectBeginning
//
//  Created by Antonina Nahnybida on 11/16/16.
//  Copyright © 2016 Andrew Nahnybida. All rights reserved.
//

import UIKit

class Statistics: UIViewController {

    @IBOutlet weak var gamesPlayed: UILabel!
    
    @IBOutlet weak var pointsScored: UILabel!
    
    @IBAction func backButton() {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    var timesPlayed = 0
    
    var totalPoints = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gamesPlayed.text = timesPlayed.description
        
        pointsScored.text = totalPoints.description
        
    }
}

//
//  LostArcade.swift
//  ProjectBeginning
//
//  Created by Antonina Nahnybida on 11/18/16.
//  Copyright © 2016 Andrew Nahnybida. All rights reserved.
//

import UIKit

class LostArcade: UIViewController {

    @IBOutlet weak var phraseLabel: UILabel!
    
    @IBOutlet weak var labelForScore: UILabel!
    
    var score = 0.0
    
    @IBAction func returnToMenu() {
        
        _ = navigationController!.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelForScore.text = score.roundTo(places: 1).description
    }
}

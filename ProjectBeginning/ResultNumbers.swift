//
//  ResultNumbers.swift
//  ProjectBeginning
//
//  Created by Antonina Nahnybida on 11/19/16.
//  Copyright © 2016 Andrew Nahnybida. All rights reserved.
//

import UIKit

class ResultNumbers: UIViewController {

    var score = 0.0
    
    @IBOutlet weak var scoreShower: UILabel!
    
    @IBOutlet weak var phraseLabel: UILabel!
    
    @IBAction func backToMenu() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scoreShower.text = score.description
        
        if score < 12.0 {
            phraseLabel.text = "You have ENTERED GOD MODE!"
        } else {
            if score < 20.0 {
                phraseLabel.text = "You are a monster in this mode!"
            } else {
                if score < 28.0 {
                    phraseLabel.text = "You are doing quite well in this mode!"
                } else {
                    phraseLabel.text = "You need to practice a bit more!"
                }
            }
        }
        
    }
}

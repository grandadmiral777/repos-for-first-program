//
//  NumbersGame.swift
//  ProjectBeginning
//
//  Created by Antonina Nahnybida on 11/6/16.
//  Copyright © 2016 Andrew Nahnybida. All rights reserved.
//

import UIKit

class NumbersGame: UIViewController {

    @IBAction func backButton() {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    var hosPos = 10
    
    var verPos = 70
    
    var counter = 0.0
    
    var timer = Timer()
    
    @IBOutlet weak var labelForTimer: UILabel!
    
    @IBAction func startTimer(_ sender: UIButton) {
        
        sender.isHidden = true
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        for _ in 0..<6 {
            for _ in 0..<4 {
                drawButton()
                hosPos = hosPos + 82
            }
            hosPos = hosPos - 82*4
            verPos = verPos + 76
        }
    }
        
    func timerAction() {
        counter = counter + 0.1
        let x = counter.roundTo(places: 1).description
        labelForTimer.text = x
    }
    
    var i = 1
    
    var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
    
    func drawButton() {
        
        let arrayKey = Int(arc4random_uniform(UInt32(numbers.count)))
        
        let randomNumber = numbers[arrayKey]
        
        let button = UIButton(frame: CGRect(x: hosPos, y: verPos, width: Int(view.frame.size.width)/6, height: Int(view.frame.size.width)/6))
        button.backgroundColor = UIColor(red: 179/255, green: 161/255, blue: 255/255, alpha: 1.0)
        button.setTitle(randomNumber.description, for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(foo(sender:)), for: .touchUpInside)
        button.addTarget(self, action: #selector(checkingButton), for: .touchUpInside)
        button.tag = randomNumber
        view.addSubview(button)
        
        numbers.remove(at: arrayKey)
    }
    
    func checkingButton(sender: UIButton) {
        
        if sender.tag == i {
            if sender.tag == 24 {
                performSegue(withIdentifier: "wonNumbers", sender: nil)
            }
            i = i + 1
        } else {
            performSegue(withIdentifier: "lostNumbers", sender: nil)
        }
    }
    
    func foo(sender: UIButton) {
        sender.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let resultVC = segue.destination as? ResultNumbers {
            resultVC.score = counter
        }
        
        if let vcRESULT = segue.destination as? LostNumbers {
            vcRESULT.score = counter
        }
    }
}

//
//  ArcadeGame.swift
//  ProjectBeginning
//
//  Created by Antonina Nahnybida on 11/6/16.
//  Copyright © 2016 Andrew Nahnybida. All rights reserved.
//

import UIKit

class ArcadeGame: UIViewController {
    
    @IBOutlet weak var timerForArcade: UILabel!
    
    var timer = Timer()
    
    @IBAction func backButton() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    var counter = 0.0
    
    var schet = 1
    
    var n = 0.3
    
    @IBOutlet weak var schetLabel: UILabel!
    
    @IBAction func startButton(_ sender: UIButton) {
        
        let loseButton = UIButton(frame: CGRect(x: 10, y: 50, width: 258, height: 466))
        loseButton.addTarget(self, action: #selector(lostArcadeGame), for: .touchUpInside)
        view.addSubview(loseButton)
        
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
        sender.isHidden = true
        
        makeArcadeWork()
    }
    
    func timerAction() {
        counter = counter + 0.1
        let x = counter.roundTo(places: 1).description
        timerForArcade.text = x
    }
    
    func lostArcadeGame() {
        performSegue(withIdentifier: "lostArcade", sender: nil)
    }
    
    func makeArcadeWork() {
        perform(#selector(makeArcadeWork), with: nil, afterDelay: n)
        drawButton()
    }
    
    func schetMaking() {
        schetLabel.text = schet.description
        if schet == 100 {
            timer.invalidate()
            performSegue(withIdentifier: "showResultOfArcade", sender: nil)
        }
        schet = schet + 1
    }
    
    func drawButton() {
        
        let hosPos = Int(arc4random_uniform(UInt32(view.frame.size.width) - UInt32(view.frame.size.width)/3) + UInt32(view.frame.size.width)/10) + Int(view.frame.size.width)/18
        let verPos = Int(arc4random_uniform(UInt32(view.frame.size.height) - UInt32(view.frame.size.height)/4 - UInt32(view.frame.size.height)/25)) + Int(view.frame.size.height)/11
        let button = UIButton(frame: CGRect(x: hosPos, y: verPos, width: Int(view.frame.size.width)/6, height: Int(view.frame.size.width)/6))
        button.backgroundColor = UIColor(red: 255/255, green: 129/255, blue: 159/255, alpha: 1.0)
        button.addTarget(self, action: #selector(foo(sender:)), for: .touchUpInside)
        view.addSubview(button)
    }
    
    func foo(sender: UIButton) {
        sender.isHidden = true
        schetMaking()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let resultVC = segue.destination as? ResultArcade {
            resultVC.score = counter
        }
        
        if let vcRESULT = segue.destination as? LostArcade {
            vcRESULT.score = counter
        }
    }
    
}

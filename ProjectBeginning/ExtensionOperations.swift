//
//  ExtensionOperations.swift
//  ProjectBeginning
//
//  Created by Antonina Nahnybida on 11/11/16.
//  Copyright © 2016 Andrew Nahnybida. All rights reserved.
//

import UIKit

class ExtensionOperations: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

//
//  ResultArcade.swift
//  ProjectBeginning
//
//  Created by Antonina Nahnybida on 11/16/16.
//  Copyright © 2016 Andrew Nahnybida. All rights reserved.
//

import UIKit

class ResultArcade: UIViewController {
    
    @IBOutlet weak var phraseLabel: UILabel!
    
    @IBOutlet weak var labelForScore: UILabel!
    
    var score = 0.0
    
    @IBAction func playAgain() {
        navigationController!.popViewController(animated: true)
    }
    
    @IBAction func returnToMenu() {
        _ = navigationController!.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelForScore.text = score.roundTo(places: 1).description
        
        if score < 30.0 {
            phraseLabel.text = "You have ENTERED GOD MODE!"
        } else {
            if score < 40.0 {
                phraseLabel.text = "You are a monester in this mode!"
            } else {
                if score > 50.0 {
                    phraseLabel.text = "You are doing quite well in this mode!"
                } else {
                        phraseLabel.text = "You need to practice a bit more!"
                }
            }
        }
        
    }
}
